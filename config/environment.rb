# Load the rails application
require File.expand_path('../application', __FILE__)

Pasantias::Application.configure do
  config.less.paths << "#{Rails.root}/lib/less/protractor/stylesheets"
  config.less.compress = true
end

# Initialize the rails application
Pasantias::Application.initialize!
